import path from "path";
import * as grpc from "@grpc/grpc-js";
import * as protoLoader from "@grpc/proto-loader";


import { ProtoGrpcType } from './Proto/ChatProto'

import { ChatAppHandlers } from './Proto/chatProto/ChatApp'

const PORT = 8082;
const PROTO_FILE_DIR = './Proto/ChatProto.proto'

const packageDef = protoLoader.loadSync(path.resolve(__dirname, PROTO_FILE_DIR));
const grpcObj = grpc.loadPackageDefinition(
    packageDef
) as unknown as ProtoGrpcType;
const chatPackage = grpcObj.chatProto;


function main() {
    const server = getServer();
    server.bindAsync(
        `0.0.0.0:${PORT}`,
        grpc.ServerCredentials.createInsecure(),
        (err, port) => {
            if (err) {
                console.error(err);
                return;
            }
            console.log(`Your server as started on port ${port}`);
            server.start();
        }
    );
}



const getServer = () => {
    const server = new grpc.Server();

    server.addService(chatPackage.ChatApp.service, {
        "allChats": (req, res) => {
            console.log(req.request)
            res(null, { message: "Im from the server" })
        }
    } as ChatAppHandlers)
    return server
}

main()